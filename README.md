# Frontend React application in TypeScript #2

## Installation

```
git clone https://gitlab.com/StandaBacak/creditas-test-project.git
yarn install
```

## Test

-   `yarn test` will run all tests

## Development

-   `yarn start`


**Description:**
1. Create a simple React application - you can use _Create React App_ - [https://facebook.github.io/create-react-app/](https://facebook.github.io/create-react-app/).
2. After the start the page will fetch the list of countries from the GraphQL API.
   - URL: [https://countries.trevorblades.com/graphql](https://countries.trevorblades.com/graphql)
   - Documentation: [https://studio.apollographql.com/public/countries/](https://studio.apollographql.com/public/countries/)
3. List of all countries will be displayed in the table with columns:
   - Name
   - Capital
   - Emoji (flag)
   - Currency
   - Actions
4. Attach a simple button to "Actions" column with trash icon to delete selected row.
5. Add paging to the end of the page and view rows per 10 records.

**Required dev stack:**
+ React (+ Hooks)
+ TypeScript
+ GraphQL
+ Styled Components

**Time severity:**
* It shouldn't take more than two hours to complete it.

**Bonus points:**
+ Nice commit messages
+ Nice graphic design
+ Translation to English and other language version
+ Tests
+ README

**Aditional notes:**
- Best way how to share the code with us is to create new GitHub/GitLab repo.
- You don't have to finish everything. If you don't, just put there a comment describing your intent and we can then discuss it later together.
