import React from 'react';
import ReactDOM from 'react-dom/client';

import StoreProvider from './contexts/Store/StoreProvider';
import App from './App';
import './i18n';

const root = ReactDOM.createRoot(
  document.getElementById('root') as HTMLElement
);
root.render(
  <React.StrictMode>
    <React.Suspense fallback="loading..">
      <StoreProvider>
        <App />
      </StoreProvider>
    </React.Suspense>
  </React.StrictMode>
);
