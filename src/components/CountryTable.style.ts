import styled from 'styled-components';

export const Table = styled.table`
  width: 80%;
  margin-top: 10px;
  table-layout: fixed;
`;

export const Tbody = styled.tbody`
  width: 100%;
  background: rgba(255, 255, 255, 0.7);
  border: 1px solid rgba(255, 255, 255, 0.3);
`;

export const HeaderTd = styled.td`
  background-color: #54585d;
  padding: 15px;
  color: #ffffff;
  font-weight: bold;
  font-size: 15px;
  border: 1px solid #54585d;
`;

export const BodyTd = styled.td`
  padding: 15px 15px;
  text-align: left;
  font-weight: 500;
  font-size: 17px;
  white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;
  color: #000000;
`;

export const TableRow = styled.tr`
  &:nth-child(odd) {
    background: #d7e5f0;
  }
`;
