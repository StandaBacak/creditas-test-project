import renderer from 'react-test-renderer';
import TableFooter from '../TableFooter';

it('renders correctly', () => {
  const tree = renderer
    .create(<TableFooter range={[]} setPage={function (page: number): void {}} page={0} slice={[]} />)
    .toJSON();
  expect(tree).toMatchSnapshot();
});
