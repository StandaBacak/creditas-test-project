import renderer from 'react-test-renderer';
import CountryTable from '../CountryTable';

it('renders correctly', () => {
  const tree = renderer
    .create(
      <CountryTable
        data={[]}
        showPopup={function (code: string): void {}}
        loading={false}
      />
    )
    .toJSON();
  expect(tree).toMatchSnapshot();
});
