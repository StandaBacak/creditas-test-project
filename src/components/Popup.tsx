import { useTranslation } from 'react-i18next';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faTimes } from '@fortawesome/free-solid-svg-icons';

import {
  PopupConteiner,
  PopupOpen,
  PopupHeader,
  CloseButton,
  ButtonsContainer,
  PopupButton,
} from './Popup.style';
interface PopupProps {
  closePopup: () => void;
  handleDelete: (code: string) => void;
  code: string;
}

const Popup = ({ closePopup, handleDelete, code }: PopupProps) => {
  const { t } = useTranslation();

  return (
    <PopupConteiner>
      <PopupOpen>
        <PopupHeader>{t('popupMsg')}</PopupHeader>
        <CloseButton onClick={() => closePopup()}>
          <FontAwesomeIcon icon={faTimes} />
        </CloseButton>
        <ButtonsContainer>
          <PopupButton onClick={() => handleDelete(code)}>
            {t('yes')}
          </PopupButton>
          <PopupButton onClick={() => closePopup()}>{t('no')}</PopupButton>
        </ButtonsContainer>
      </PopupOpen>
    </PopupConteiner>
  );
};

export default Popup;
