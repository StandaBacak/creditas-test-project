import styled from 'styled-components';

export const PopupConteiner = styled.div`
  position: fixed;
  width: 350px;
  height: 150px;
  margin: 50px;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  margin: auto;
  border: 1px solid black;
  border-radius: 20px;
  background-color: rgba(0, 0, 0, 0.5);
`;

export const PopupOpen = styled.div`
  margin: auto;
  width: 100%;
  height: 100%;
  border-radius: 20px;
  background: #ffffff;
`;

export const PopupHeader = styled.div`
  text-align: center;
  font-family: Arial, Helvetica, sans-serif;
  color: #000000;
  padding-top: 20px;
`;

export const CloseButton = styled.button`
  position: absolute;
  right: 10px;
  top: 10px;
  border: 0px;
  border-radius: 20px;
  font-size: 12px;
  padding: 5px 8px;
`;

export const ButtonsContainer = styled.div`
  text-align: center;
  margin: auto;
  width: 50%;
  padding-top: 50px;
`;

export const PopupButton = styled.button`
  margin-left: 20px;
  margin-right: 20px;
  padding: 5px;
  border-radius: 5px;
  border: 1px solid #000000;
`;
