import styled from 'styled-components';

export const Footer = styled.div`
  background-color: #f1f1f1;
  padding: 8px 0px;
  width: 80%;
  font-weight: 500;
  text-align: left;
  font-size: 16px;
  color: #2c3e50;
  border-bottom-left-radius: 15px;
  border-bottom-right-radius: 15px;
  display: flex;
  align-items: center;
  justify-content: center;
`;

export const FooterButtonWrapper = styled.div`
  width: 50px;
  display: flex;
  align-items: center;
  justify-content: center;
  padding: 3px 8px;
  margin-right: 4px;
  margin-left: 4px;
`;

export const PageNumber = styled.div`
  width: 20px;
  display: flex;
  align-items: center;
  justify-content: center;

`;
