import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faArrowRight, faArrowLeft } from '@fortawesome/free-solid-svg-icons';

import { Footer, FooterButtonWrapper, PageNumber } from './TableFooter.style';

import type { Country } from '../../contexts/Store/StoreProvider';

function TableFooter({
  range,
  setPage,
  page,
  slice,
}: {
  range: number[];
  setPage: (page: number) => void;
  page: number;
  slice: Country[];
}) {
  return (
    <Footer>
      <FooterButtonWrapper>
        {range[0] !== page && (
          <button onClick={() => setPage(page - 1)}>
            <FontAwesomeIcon icon={faArrowLeft} />
          </button>
        )}
      </FooterButtonWrapper>
      <PageNumber>{page}</PageNumber>
      <FooterButtonWrapper>
        {range.length !== page && (
          <button onClick={() => setPage(page + 1)}>
            <FontAwesomeIcon icon={faArrowRight} />
          </button>
        )}
      </FooterButtonWrapper>
    </Footer>
  );
}

export default TableFooter;
