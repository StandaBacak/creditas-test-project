import { useState } from 'react';
import { useTranslation } from 'react-i18next';
import Button from 'react-bootstrap/Button';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faTrash } from '@fortawesome/free-solid-svg-icons';

import { Table, Tbody, HeaderTd, TableRow, BodyTd } from './CountryTable.style';
import TableFooter from './TableFooter';
import { TABLE_HEADER, ROW_PER_PPAGE } from '../constants';
import useTable from '../hooks/useTable';

import type { Country } from '../contexts/Store/StoreProvider';

function CountryTable({
  data,
  showPopup,
  loading,
}: {
  data: Country[];
  showPopup: (code: string) => void;
  loading: boolean;
}) {
  const [page, setPage] = useState(1);
  const { slice, range } = useTable(data, page, ROW_PER_PPAGE);
  const { t } = useTranslation();

  return (
    <>
      <Table>
        <thead>
          <tr>
            {TABLE_HEADER.map((row, index) => (
              <HeaderTd key={index}>{t(row.key)}</HeaderTd>
            ))}
          </tr>
        </thead>

        <Tbody>
          {slice.map((country, index) => (
            <TableRow key={index}>
              <BodyTd>{country.name}</BodyTd>
              <BodyTd>{country.capital}</BodyTd>
              <BodyTd>{country.emoji}</BodyTd>
              <BodyTd>{country.currency}</BodyTd>
              <BodyTd>
                <Button onClick={() => showPopup(country.code)}>
                  <FontAwesomeIcon icon={faTrash} />
                </Button>
              </BodyTd>
            </TableRow>
          ))}
        </Tbody>
      </Table>
      <TableFooter range={range} slice={slice} setPage={setPage} page={page} />
    </>
  );
}

export default CountryTable;
