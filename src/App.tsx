import { useTranslation } from 'react-i18next';
import { createGlobalStyle } from 'styled-components';

import useStore from './contexts/Store/useStore';
import Popup from './components/Popup';  
import CountryTable from "./components/CountryTable";
import { CZECH_LANGUAGE, ENGLISH_LANGUAGE } from "./constants"

const GlobalStyle = createGlobalStyle`
  body {
    background: -webkit-linear-gradient(left, #3a43bd, #25b7c4);
    background: linear-gradient(to right, #344cc2, #25b7c4);
  }
`
interface LanguageName {
  nativeName: string;
}
interface LanguageInfo {
  en: LanguageName;
  cs:  LanguageName;
}

const lngs: LanguageInfo = {
  en: { nativeName: ENGLISH_LANGUAGE},
  cs: { nativeName: CZECH_LANGUAGE},
}

function App() {
  const { countries, loading, showPopup, hidePopup, openPopup, deleteCountry, currentCode } = useStore();

  const { i18n, t } = useTranslation();

  return (
    <div>
      <h1>{t('tableName')}</h1>  
        {openPopup ?  
        <Popup  
          closePopup={hidePopup}
          handleDelete={deleteCountry}
          code={currentCode}  
        />  
        : null  
        }  
      <div>
        {Object.keys(lngs).map((lng) => (
          <button type="submit" key={lng} onClick={() => i18n.changeLanguage(lng)} disabled={i18n.resolvedLanguage === lng}>{lngs[lng as keyof LanguageInfo].nativeName}</button>
        ))}
       
      </div>
      <CountryTable data={countries} showPopup={showPopup} loading={loading} />
      <GlobalStyle />
    </div>
  );
}

export default App;