export const CZECH_LANGUAGE = 'Česky';

export const ENGLISH_LANGUAGE = 'English';

export const TABLE_HEADER: { key: string; label: string }[] = [
  { key: 'name', label: 'name' },
  { key: 'capital', label: 'capital' },
  { key: 'emoji', label: 'flag' },
  { key: 'currency', label: 'currency' },
  { key: 'actions', label: 'actions' },
];

export const ROW_PER_PPAGE = 10;

export const API_URL = 'https://countries.trevorblades.com';
