import { useCallback, useEffect, useState } from 'react';
import { ApolloClient, InMemoryCache, gql, useQuery } from '@apollo/client';

import StoreContext from './StoreContext';
import { API_URL } from '../../constants';

export interface Country {
  id: number;
  name: string;
  capital: string;
  emoji: string;
  currency: string;
  code: string;
}

const client = new ApolloClient({
  cache: new InMemoryCache(),
  uri: API_URL,
});

const LIST_COUNTRIES = gql`
  {
    countries {
      name
      code
      currency
      capital
      emoji
    }
  }
`;

const StoreProvider = ({ children }: { children: JSX.Element }) => {
  const [countries, setCountries] = useState<Country[]>([]);
  const { data, loading } = useQuery(LIST_COUNTRIES, { client });
  const [openPopup, setOpenPopup] = useState(false);
  const [currentCode, setCurrentCode] = useState('');

  const hidePopup = useCallback(() => {
    setOpenPopup(false);
  }, []);

  const showPopup = useCallback((code: string) => {
    setCurrentCode(code);
    setOpenPopup(true);
  }, []);

  const deleteCountry = (code: string) => {
    const filteredCountries = countries.filter((country: Country) => {
      return country.code !== code;
    });

    setCountries(filteredCountries);
    hidePopup();
  };

  useEffect(() => {
    if (!loading) setCountries(data.countries);
  }, [data, loading]);

  return (
    <StoreContext.Provider
      value={{
        setCountries,
        countries,
        loading,
        showPopup,
        hidePopup,
        openPopup,
        deleteCountry,
        currentCode,
      }}
    >
      {children}
    </StoreContext.Provider>
  );
};

export default StoreProvider;
