import React from 'react';

import type { Country } from './StoreProvider';

interface StoreContextType {
  countries: Country[];
  setCountries: (data: Country[]) => void;
  loading: boolean;
  showPopup: (code: string) => void;
  hidePopup: () => void;
  openPopup: boolean;
  deleteCountry: (code: string) => void;
  currentCode: string;
}

const StoreContext = React.createContext<StoreContextType>(
  {} as StoreContextType
);

export default StoreContext;
